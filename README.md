# This is the sample API to be used during session 5's demo lesson

It is left intentionally incomplete. You will need to add your own MongoDB Atlas connection string and link your order routes in app.js, as well as add the model, controller, and routes files created during sessions 3 and 4 in their appropriate folders.

- If any details regarding usage is unclear, ask your trainer.